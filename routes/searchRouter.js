/**
 * @search teacher and students
 */
const router = require('express').Router();
const { searchTeacher, searchUser } = require('../controllers/search.controller');
const { ensureAdmin } = require('../middleware/auth');

router.get('/teacher', searchTeacher);
router.get('/user', ensureAdmin, searchUser);

module.exports = router;
